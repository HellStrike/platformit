using Godot;

public class Player : KinematicBody2D
{
    private const float GRAVITY = 50;
    private const float SPEED = 300;
    private const float JUMP_SPEED = 1000;

    private Vector2 _velocity = Vector2.Zero;
    private AnimationNodeStateMachinePlayback stateMachine;
    private Sprite sprite;

    private int _score = 0;

    public override void _Ready()
    {
        sprite = GetNode<Sprite>("Sprite");
        var animTree = GetNode<AnimationTree>("AnimationTree");
        stateMachine = (Godot.AnimationNodeStateMachinePlayback)animTree.Get("parameters/playback");
    }

    public override void _Input(InputEvent @event)
    {
        if (@event.IsActionPressed("ui_right"))
        {
            _velocity.x = SPEED;
            sprite.FlipH = false;
        }

        if (@event.IsActionReleased("ui_right"))
        {
            if (_velocity.x > 0)
                _velocity.x = 0;
        }

        if (@event.IsActionPressed("ui_left"))
        {
            _velocity.x = -SPEED;
            sprite.FlipH = true;
        }

        if (@event.IsActionReleased("ui_left"))
        {
            if (_velocity.x < 0)
                _velocity.x = 0;
        }

        if (@event.IsActionPressed("ui_select"))
        {
            if (IsOnFloor())
                _velocity.y = -JUMP_SPEED;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        _velocity = MoveAndSlide(_velocity, Vector2.Up);

        if(IsOnFloor())
        {
            if (_velocity.Length() > 0)
                stateMachine.Travel("Move");
            else
                stateMachine.Travel("Idle");
        }
        else
        {
            stateMachine.Travel("Jump");
        }

        _velocity.y += GRAVITY;
    }

    public void IncScore() 
    { 
        _score++;
        GD.Print(_score);
    }
}
