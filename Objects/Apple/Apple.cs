using Godot;
using System;

public class Apple : Area2D
{
    public void OnAppleBodyEntered(Node body)
    {
        if (body.IsInGroup("Player"))
        {
            Player p = (Player)body;
            p.IncScore();
            QueueFree();
        }
    }
}
